import React, { useState, useEffect } from 'react';
import { Accordion, Badge, Button, Card, Col, Form, ListGroup, Row } from 'react-bootstrap';
import Board from 'react-trello';
import { BASE_URL } from '../../providers/base-url';
import BoardApi from '../Base/Api/BoardApi';
import Comment from './Comment';
import PropTypes from 'prop-types';
import NewCardForm from './Components/NewCardForm';
import NewLane from './Components/NewLaneForm';
import NewLaneSection from './Components/NewLaneSection';
import './SingleBoard.css'
import './Scrollbar.css'
import { BsImage, BsFillTrashFill } from 'react-icons/bs';
import ChangeBackground from './ChangeBackground';
import { Link } from 'react-router-dom';

const SingleBoard = ({ match, history }) => {
  const {
    params: { id }
  } = match;

  const [board, setBoard] = useState([])
  const [boardData, setBoardData] = useState(null)
  const theme = localStorage.getItem('theme')
  const [comments, setComments] = useState(board.comments || []);
  const [comment, setComment] = useState(null);
  const [email, setEmail] = useState(null);
  const [users, setUsers] = useState(board.users || []);
  const [backgroundForm, showBackgroundForm] = useState(false)
  const [backgroundUrl, setBackgroundUrl] = useState('');
  const [backgroundHasChanged, setBackgroundChanged] = useState(false);

  useEffect(() => {
    fetch(`${BASE_URL}/board/${id}`,
      {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`,
        }
      })
      .then(result => {
        return result.json();
      })
      .then(result => {
        if (result) {
          result.lanes.sort((a, b) => a.id - b.id);
          const styledLanes = result.lanes.map((lane) => {
            lane.style = {
              backgroundColor: localStorage.getItem('theme') === 'light' ? '#363537' : '#ffc17a',
              color: localStorage.getItem('theme') === 'light' ? 'white' : 'black'
            }
            return lane;
          })
          result.lanes = [...styledLanes];
          setBoard(result);
          setBoardData({ lanes: result.lanes });
          setComments(result.comments)
          setBackgroundUrl(result.backgroundUrl);
        }
      })
      .catch(err => console.log(err.message));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleDeleteBoard = () => {
    BoardApi.removeBoard(board.id);
    history.push('/boards');
    window.location.reload(true);
  }

  const addComment = () => {
    fetch(`${BASE_URL}/board/${id}/comment`, {
      method: 'POST',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify({ comment: comment })
    })
      .then(responce => responce.json())
      .then(result => {
        setComments(comments.push(result));
      })
      .catch(err => console.log(err.message))
  }

  const addUser = () => {
    fetch(`${BASE_URL}/board/${id}/users`, {
      method: 'PUT',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify({ email: email })
    })
      .then(responce => responce.json())
      .then(result => {
        setUsers(users.push(result));
      })
      .catch(err => console.log(err.message))
      .finally(window.location.reload(false))
  }


  const onCardDragEnd = (cardId, sourceLaneId, targetLaneId, position, cardDetails) => {
    if (sourceLaneId === targetLaneId && cardDetails.position === position) {
      return false;
    }

    const cardUpdateDto = {
      id: cardId,
      title: cardDetails.title,
      description: cardDetails.description,
      label: cardDetails.label,
      laneId: +targetLaneId,
      position: position,
      sourceLaneId: +sourceLaneId
    }
    const result = BoardApi.updateCard(id, cardUpdateDto);
    console.log(result);
  }

  const handleCardAdd = (card, laneId) => {

    const cardAddDto = {
      title: card.title || '',
      description: card.description || '',
      label: card.label || '',
      laneId: +laneId
    }
    fetch(`${BASE_URL}/board/${id}/cards`, {
      method: 'POST',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify(cardAddDto)
    })
      .then(response => response.json())
      .then(result => {
        console.log('Card Added');
        const copyBoard = { ...board };
        copyBoard.lanes.find(x => x.id === result.laneId).cards.push(result);
        setBoardData({ lanes: copyBoard.lanes });
      })
      .catch(err => console.log(err.message))
  }

  const handleCardDelete = (cardId, laneId) => {
    const cardDto = {
      id: +cardId,
      laneId: +laneId
    }
    BoardApi.removeCard(id, cardDto);
  }

  const handleSubmitLane = (params) => {
    const laneDto = {
      title: params.title,
      label: '0/0'
    }
    BoardApi.addLane(id, laneDto);
    window.location.reload(true);
  }

  const handleDeleteLane = (laneId) => {
    BoardApi.removeLane(id, { id: +laneId });
  }

  const togglesUpdateBackgroundForm = () => {
    if (backgroundHasChanged) {
      showBackgroundForm(!backgroundForm);
      window.location.reload(true);
    } else {
      showBackgroundForm(!backgroundForm);
    }
  };

  const toggleBackgroundHasChanged = () => {
    setBackgroundChanged(true);
  }

  if (backgroundForm) {
    return (<ChangeBackground close={togglesUpdateBackgroundForm} hasChanged={toggleBackgroundHasChanged} id={board.id} currentBackgroundUrl={backgroundUrl} />)

  }

  return (
    <div style={{ margin: '2%', marginBottom: '5%' }} className='justify-content-md-center'>


      <Row className='justify-content-md-center'>
        <Col xs={12} md={3}>
          <BsImage size={32} onClick={togglesUpdateBackgroundForm} /> Background
      </Col>
        <Col>
          <h1 style={{ fontFamily: 'Raleway' }}> {board.name}</h1>
        </Col>
        <div style={{ float: 'right', marginRight: '15px' }}>
          <BsFillTrashFill size={32} onClick={handleDeleteBoard} />
        </div>
      </Row>
      <br />
      {boardData ?
        <Row className='justify-content-md-center'>
          <Col xs={12} md={8} xl={9}>
            <Board
              data={boardData}
              draggable={true}
              collapsibleLanes={true}
              editable={true}
              canAddLanes={true}
              handleDragEnd={onCardDragEnd}
              style={theme === 'light' ?
                {
                  backgroundColor: '#EFDCEB',
                  maxWidth: '100%',
                  backgroundImage: `url(${BASE_URL}/${board.backgroundUrl})`
                } :
                {
                  backgroundColor: '#363537',
                  maxWidth: '100%',
                  backgroundImage: `url(${BASE_URL}/${board.backgroundUrl})`
                }}
              laneStyle={{ backgroundColor: '#FAFAFA' }}
              cardStyle={{ fontFamily: 'Raleway', padding: '10px', backgroundColor: '#E2E2E2', border: '1px solid darkorange' }}
              components={{ NewCardForm: NewCardForm, NewLaneForm: NewLane, NewLaneSection: NewLaneSection }}
              onCardAdd={handleCardAdd}
              onCardDelete={handleCardDelete}
              onLaneAdd={handleSubmitLane}
              onLaneDelete={handleDeleteLane}
              className='scrollbar scrollbar-primary'>
            </Board></Col>
          <Col xs={12} md={4} xl={3}>
            <Accordion
              defaultActiveKey='1'
              style={{
                borderLeft: '1px solid gray',
                padding: '10px', width: '100%',
                fontFamily: 'Raleway'
              }}>
              <Card>
                <Accordion.Toggle
                  as={Button}
                  variant='link'
                  eventKey='0'
                  style={{ color: '#900C3F', backgroundColor: '#FAFAFA' }}>
                  Members
                </Accordion.Toggle>
                <Accordion.Collapse
                  eventKey="0"
                  style={{
                    backgroundColor: '#FAFAFA',
                    padding: '5%'
                  }}>
                  <>
                    <div
                      className='scrollbar scrollbar-primary'
                      style={{ maxHeight: '300px' }}>
                      {board.users.map(user =>
                        <ListGroup.Item style={{ fontSize: '10pt', height: '80% ' }} key={user.id}>
                          <Link to={`/profile/${user.id}`} href={`/profile/${user.id}`}>
                            {user.firstName} {user.lastName}
                          </Link>
                        </ListGroup.Item>)
                      }
                    </div>
                    <br />
                    <Row className="justify-content-md-center" style={{ textAlign: 'center' }}>
                      <Col>
                        <Form.Control
                          type='email'
                          as='textarea'
                          value={email || ''}
                          onChange={e => setEmail(e.target.value)}
                          style={{
                            fontSize: '10pt',
                            height: '30px',
                            width: '100%'
                          }} />
                        <Badge
                          onClick={addUser}
                          style={{
                            cursor: 'pointer',
                            color: 'black',
                            marginLeft: '-10px'
                          }}>
                          add user
                            </Badge>
                      </Col>
                    </Row>
                  </>
                </Accordion.Collapse>
              </Card>
            </Accordion>
            <Accordion
              defaultActiveKey='1'
              style={{
                borderLeft: '1px solid gray',
                padding: '10px',
                width: '100%',
                fontFamily: 'Raleway'
              }}>
              <Card>
                <Accordion.Toggle
                  as={Button}
                  variant='link'
                  eventKey='0'
                  style={{
                    color: '#900C3F',
                    backgroundColor: '#FAFAFA',
                  }}>
                  Comments
                  </Accordion.Toggle>
                <Accordion.Collapse
                  eventKey='0'
                  style={{
                    backgroundColor: '#E2E2E2',
                    padding: '5%'
                  }}>
                  <>
                    <div className='scrollbar scrollbar-primary' style={{ maxHeight: '550px' }}>
                      {board.comments.map(comment => comment.id % 2 === 0 ?
                        <Comment comment={comment} key={comment.id} variant="2" /> :
                        <Comment comment={comment} key={comment.id} variant="1" />)}
                    </div>
                    <Form
                      style={{ paddingBottom: '5%', paddingLeft: '3%' }}>
                      <Row>
                        <Col xs={9}>
                          <Form.Control
                            type='text'
                            as='textarea'
                            value={comment || ''}
                            onChange={e => setComment(e.target.value)}
                            style={{
                              fontSize: '10pt',
                              height: '50px',
                              width: '110%'
                            }} />
                        </Col>
                        <Col>
                          <br />
                          <Badge onClick={addComment}
                            style={{
                              cursor: 'pointer',
                              color: 'black',
                              marginLeft: '2px'
                            }}>
                            Add
                          </Badge>
                        </Col>
                      </Row>
                    </Form></>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </Col>
        </Row> : null}
    </div>
  )
}

export default SingleBoard;

SingleBoard.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
}