import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany, JoinTable } from "typeorm";
import { Comment } from "./comment.entity";
import { Lane } from "./lane.entity";
import { User } from "./user.entity";

@Entity()
export class Board {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column({ default: false })
  isDeleted: boolean;

  @Column({ nullable: true })
  backgroundUrl: string;

  @OneToMany(type => Lane, lane => lane.boardId)
  lanes: Lane[];

  @ManyToMany(type => User, user => user.boards)
  users: User[];

  @OneToMany(type => Comment, comment => comment.forBoard)
  comments: Comment[]
}

