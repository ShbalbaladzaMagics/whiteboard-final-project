import React, { useContext, useState } from 'react';
import { BsFillShieldLockFill, BsFillEnvelopeFill } from 'react-icons/bs'
import { Container, Form, Col, Button, InputGroup, Row } from 'react-bootstrap';
import AuthContext, { extractUser } from '../../../providers/AuthContext';
import { BASE_URL } from '../../../providers/base-url';

const Login = ({ history }) => {

  const [loggingUser, setLoggingUser] = useState({});

  const { user, setLoginState } = useContext(AuthContext);

  const updateEmail = email => setLoggingUser({ ...loggingUser, email });
  const updatePassword = password => setLoggingUser({ ...loggingUser, password });

  const login = () => {
    fetch(`${BASE_URL}/session/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(loggingUser),
    })
      .then(response => response.json())
      .then(result => {
        sessionStorage.setItem('token', result.token);

        setLoginState({
          isLoggedIn: !!extractUser(result.token),
          user: extractUser(result.token),
        });
      })
      .catch(error => console.log(error.message))
      .finally(window.localStorage.setItem('theme', user.theme))
  }

  if (user) {
    window.localStorage.setItem('theme', user.theme)
    window.location.reload(false)
    history.push('/')
  }

  return (
    <Container style={{ marginTop: '3%' }}>
      <h1 style={{ fontFamily: 'Raleway', color: '#522150' }}>
        Login
        </h1>
      <br />
      <div>
        <Row><Col xs={10} md={5}>
          <InputGroup.Prepend>
            <InputGroup.Text id='basic-addon1'> <BsFillEnvelopeFill /></InputGroup.Text>
            <Form.Control
              type='text'
              id='username'
              onChange={e => updateEmail(e.target.value)}
              placeholder='Username' />
            <br />
          </InputGroup.Prepend>
        </Col>
        </Row>
        <br />
        <Row>
          <Col xs={10} md={5}>
            <InputGroup.Prepend>
              <InputGroup.Text id='basic-addon1'>< BsFillShieldLockFill /></InputGroup.Text>
              <Form.Control
                type='password'
                placeholder='Password'
                onChange={e => updatePassword(e.target.value)}
                id='password' />
            </InputGroup.Prepend>
          </Col>
        </Row>
        <br />
        {/* <Row>
          <span style={{ paddingLeft: '1em' }}>Having troubles with logging into your account? <a href='#'>reset password</a></span>
        </Row> */}
        <br />
        <Button style={{ backgroundColor: '#522150' }} onClick={login}>Login</Button>
      </div>
    </Container>
  )
}

export default Login;