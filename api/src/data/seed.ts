import * as bcrypt from 'bcrypt';
import { createConnection, Repository, Connection } from 'typeorm';
import { User } from '../models/user.entity';
import { Board } from '../models/board.entity';

const seedUsers = async (connection: any) => {
  const usersRepository: Repository<User> = connection.manager.getRepository(User);

  const users = [{
    email: 'rachelgreen@friends.com',
    password: await bcrypt.hash('friends', 10),
    firstName: 'Rachel',
    lastName: 'Green',
    company: 'Ralph Loren',
    companyRole: 'Fashion Executive',
    bio: 'Fashion Exec. and the best-dressed person you\'ll meet today',
    avatarUrl: 'rachel.jpg'
  },
  {
    email: 'monicageller@javu.com',
    password: await bcrypt.hash('friends', 10),
    firstName: 'Monica',
    lastName: 'Geller',
    company: 'Javu Restaurant',
    companyRole: 'Head Chef',
    bio: 'If I\'m harsh with you is only because you are doing it wrong :)',
    avatarUrl: 'monica.jpg'
  },
  {
    email: 'buffay@friends.com',
    password: await bcrypt.hash('friends', 10),
    firstName: 'Phoebe',
    lastName: 'Buffay',
    company: 'Friends & Co.',
    companyRole: 'Head of Entertainment Department',
    bio: 'I have a job, but my true passion is singing. You can check my song "Smelly Cat" on Spotify! \n #SmellyCat #vegetarian #peace',
    avatarUrl: 'phoebe.jpg'
  },
  {
    email: 'joeytribbiani@friends.com',
    password: await bcrypt.hash('friends', 10),
    firstName: 'Joey',
    lastName: 'Tribbiani',
    companyRole: 'Actor',
    bio: 'How ya doin?',
    avatarUrl: 'joey.jpg'
  },
  {
    email: 'chandlerbing@friends.com',
    password: await bcrypt.hash('friends', 10),
    firstName: 'Chandler',
    lastName: 'Bing',
    company: 'Friends & Co.',
    companyRole: 'Junior Advertising Copywriter',
    bio: 'Used to work in data analysis, but decided I hate having money so am now in advertisement',
    avatarUrl: 'chandler.jpg'
  },
  {
    email: 'rossgeller@friends.com',
    password: await bcrypt.hash('friends', 10),
    firstName: 'Ross',
    lastName: 'Geller',
    company: 'Columbia University',
    companyRole: 'Professor',
    bio: 'Paleontology professor, who loves having a break somethimes.',
    avatarUrl: 'ross.jpg'
  },
  {
    email: 'janicegt@friends.com',
    password: await bcrypt.hash('friends', 10),
    firstName: 'Janice',
    lastName: 'Litman-Goralnik',
    company: 'self-employed',
    bio: 'OH MY GOD, you HAVE to check-out my own leopard clothing line! It\'s for divas like us xoxo',
    avatarUrl: 'janice.jpg'
  },
  {
    email: 'estelleleonard@friends.com',
    password: await bcrypt.hash('friends', 10),
    firstName: 'Estelle',
    lastName: 'Leonard',
    company: 'The Estelle Leonard Talent Agency',
    companyRole: 'Agent',
    bio: 'Are you the next superstar? Probably not.',
    avatarUrl: 'estelle.jpg'
  }]

  for (const user of users) {
    const foundUser = await usersRepository.findOne({
      email: user.email,
    });

    if (!foundUser) {
      const userToCreate = usersRepository.create(user);
      console.log(await usersRepository.save(userToCreate));
    }
  }
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedUsers(connection);
  console.log('Seed completed!');

  connection.close();
}

seed().catch(console.error);