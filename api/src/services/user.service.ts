import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "src/models/user.entity";
import { Repository } from "typeorm";
import { CreateUserDTO } from "src/dtos/user/user-create.dto";
import * as bcrypt from 'bcrypt'
import { TransformService } from "./transform.service";
import { ReturnUserDTO } from "src/dtos/user/user-return.dto";
import { UpdateUserDTO } from "src/dtos/user/user-update.dto";
import { ChangeThemeDTO } from "src/dtos/change-theme.dto";


@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly transform: TransformService
  ) { }

  public async createUser(user: CreateUserDTO): Promise<ReturnUserDTO> {
    user.password = await bcrypt.hash(user.password, 10);
    const newUser = await this.userRepository.save(user);

    return this.transform.toResponceUserDTO(newUser);
  }

  public async getAllUsers(): Promise<ReturnUserDTO[]> {
    const users = await this.userRepository.find();

    return users.map<ReturnUserDTO>(this.transform.toResponceUserDTO);
  }

  public async findById(id: string): Promise<ReturnUserDTO> {
    const user = await this.getUser(+id);
    return this.transform.toResponceUserDTO(user);
  }

  public async updateUser(id: string, updates: UpdateUserDTO): Promise<ReturnUserDTO> {
    const user = await this.getUser(+id);
    user.firstName = updates.firstName;
    user.lastName = updates.lastName;
    user.company = updates.company;
    user.bio = updates.bio;
    
    await this.userRepository.save(user);
    return this.transform.toResponceUserDTO(user);
  }

  async updateAvatar(id: string, filename: string) {
    const user = await this.getUser(+id);

    user.avatarUrl = filename;
    await this.userRepository.save(user);
    return this.transform.toResponceUserDTO(user);
}

  async changeTheme(id: string, newTheme: ChangeThemeDTO) {
    const user = await this.getUser(+id);
    user.theme = newTheme.theme;
    await this.userRepository.save(user);
    return this.transform.toResponceUserDTO(user);
  }


  async getUser(userId: number): Promise<User> {
    const user = await this.userRepository.findOne({
      where: {
      id: userId
    }, 
    relations: ["boards", "boards.lanes", "boards.lanes.cards", "boards.comments"]
    });

    if (!user) {
      throw new Error(`No user matches the given criteria`)
    }

    const userBoards = user.boards.filter((board) => !board.isDeleted)
    user.boards = userBoards;

    return user;
  }
  
}
