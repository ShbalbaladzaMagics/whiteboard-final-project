import React, { useState } from 'react'
import { Badge, Button, Col, Form, Row } from 'react-bootstrap';
import { BASE_URL } from '../../providers/base-url';

const AddComment = ({ id }) => {
  const [comment, setComment] = useState(null);

  const addComment = () => {
    fetch(`${BASE_URL}/board/${id}/comment`, {
      method: 'POST',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify({ comment: comment })
    })
      .then(responce => responce.json())
      .then(result => {
      })
      .catch(err => console.log(err.message))
  }


  return (
    <Form style={{ paddingBottom: '5%', paddingLeft: '3%' }}>
      <Row>
        <Col xs={9}>
          <Form.Control
            type='text'
            as='textarea'
            value={comment}
            onChange={e => setComment(e.target.value)}
            style={{
              fontSize: '10pt',
              height: '50px',
              width: '110%'
            }} />
        </Col>
        <Col>
          <br />
          <Badge
            onClick={addComment}
            style={{
              cursor: 'pointer',
              color: 'black',
              marginLeft: '-10px'
            }}>
            Add</Badge>
        </Col>
      </Row>
    </Form>
  )
}

export default AddComment;