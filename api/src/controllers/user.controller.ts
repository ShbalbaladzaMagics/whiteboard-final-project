import { Controller, Post, Body, Get, Param, UseGuards, Put, UseInterceptors, UploadedFile } from "@nestjs/common";
import { UserService } from "src/services/user.service";
import { CreateUserDTO } from "src/dtos/user/user-create.dto";
import { User } from "src/models/user.entity";
import { ReturnUserDTO } from "src/dtos/user/user-return.dto";
import { AuthGuard } from "@nestjs/passport";
import { BlacklistGuard } from "src/auth/blacklist.guard";
import { UpdateUserDTO } from "src/dtos/user/user-update.dto";
import { UserId } from "src/auth/user-id.decorator";
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from "path";
import { ChangeThemeDTO } from "src/dtos/change-theme.dto";

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Post('/register')
  async register(@Body() newUser: CreateUserDTO): Promise<ReturnUserDTO> {
    return await this.userService.createUser(newUser);
  }

  @Get()
  @UseGuards(BlacklistGuard)
  async allUsers(): Promise<ReturnUserDTO[]> {
    return this.userService.getAllUsers();
  }

  @Get('/:id')
  @UseGuards(BlacklistGuard)
  async getById(@Param('id') id: string): Promise<ReturnUserDTO> {
    return this.userService.findById(id);
  }

  @Put('/:id')
  async update(@Param('id') id: string, @Body() updates: UpdateUserDTO): Promise<ReturnUserDTO> {
 
    return this.userService.updateUser(id, updates);
  }

  @Put(':id/avatar')
  @UseInterceptors(
    FileInterceptor('files', {
      storage: diskStorage({
        destination: './avatars',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          console.log('filename cb', file);
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          //Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadAvatar(@UploadedFile() files, @Param('id') id: string) {
    return await this.userService.updateAvatar(id, files.filename);
  }

  @Put('/:id/theme')
  async themeChange(@Param('id') id: string, @Body() newTheme: ChangeThemeDTO): Promise<ReturnUserDTO> {
    return await this.userService.changeTheme(id, newTheme);
  }
}

