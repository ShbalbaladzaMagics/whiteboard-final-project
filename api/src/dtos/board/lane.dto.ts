import { Card } from "src/models/card.entity";

export class LaneDto {
  title: string;
  label: string;
}

