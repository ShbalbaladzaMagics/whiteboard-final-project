import { Injectable, UnauthorizedException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "src/models/user.entity";
import { Repository } from "typeorm";
import { JwtService } from "@nestjs/jwt";
import * as bcrypt from 'bcrypt';
import { JWTPayload } from "src/strategy/jwt-payload";
import { LoginDTO } from "src/dtos/user/login.dto";
import { Token } from "src/models/token.entity";
import { UserRole } from "src/models/enums/user-role.enum";
import { use } from "passport";

@Injectable()
export class AuthService {

  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Token) private readonly tokenRepository: Repository<Token>,
    private readonly jwtService: JwtService,
  ){}

  async findUserByName(email: string) {
    return await this.userRepository.findOne({ 
      where: {
        email
      }
     });
  }

  async validateUser(email: string, password: string) {
    const user = await this.findUserByName(email);
    if (!user) {
      return null;
    }
    const isUserValidated = await bcrypt.compare(password, user.password);
    return isUserValidated
            ? user
            : null;
  }

  async blacklist(token: string) {
    const tokenEntity = this.tokenRepository.create();
    tokenEntity.token = token;

    await this.tokenRepository.save(tokenEntity);
  }

  async isBlacklisted(token: string): Promise<boolean> {
    return Boolean(await this.tokenRepository.findOne({
      where: {
        token: token
      }
    })
    )
  }

  async login(loginUser: LoginDTO): Promise<{ token: string }> {
    const user = await this.validateUser(loginUser.email, loginUser.password);

    if (!user) {
      throw new UnauthorizedException('Wrong credentials!');
    }

    const payload: JWTPayload = {
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      theme: user.theme
    }

    const token = await this.jwtService.signAsync(payload);

    return {
      token,
    };
  }

}
