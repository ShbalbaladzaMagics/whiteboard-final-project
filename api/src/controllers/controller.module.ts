import { Module } from "@nestjs/common";
import { ServiceModule } from "src/services/service.module";
import { UserController } from "./user.controller";
import { AuthController } from "./auth.controller";
import { BoardController } from "./board.controller";

@Module({
  imports: [ServiceModule],
  controllers: [UserController, AuthController, BoardController]
})

export class ControllerModule { }