import React, { useState } from 'react';
import { BsX } from 'react-icons/bs';
import { Button, Container, Form, Row } from 'react-bootstrap';
import { BASE_URL } from '../../providers/base-url';

const ChangeBackground = ({ close, hasChanged, id, currentBackgroundUrl }) => {
  const [files, setFiles] = useState([]);
  const [currentUrl, setUrl] = useState(currentBackgroundUrl);

  const change = () => {
    if (files.length === 0) {
      alert('Please upload a file!');
      return;
    }
    const data = new FormData();
    data.append('files', files[0]);
    fetch(`${BASE_URL}/board/${id}/background`, {
      method: 'PUT',
      headers: {
      },
      body: data,
    })
      .then(r => r.json())
      .then(result => {
        setUrl(result.backgroundUrl);
        hasChanged();
      })
      .catch(console.warn)
  };

  return (
    <>
      <BsX
        style={{ float: 'left' }}
        size={32}
        onClick={close} />
      <Container style={{ marginTop: '3%' }}>
        <Row className='justify-content-md-center'>
          <h1 style={{ fontFamily: 'Raleway', marginBottom: '40px' }}>
            Choose background for this board...
            </h1>
        </Row>
        <Row className='justify-content-md-center'>
          <Form.File type='file' onChange={e => setFiles(Array.from(e.target.files))} />
          {localStorage.getItem('theme') === 'light' ?
            <Button variant='warning' onClick={change}> Change </Button> :
            <Button variant='outline-warning' onClick={change}> Change </Button>
          }
          {
            currentUrl &&
            <div style={{
              width: '80%',
              height: '750px',
              backgroundImage: `url(${BASE_URL}/${currentUrl}`
            }} />
          }
        </Row>
      </Container >
    </>
  )
}

export default ChangeBackground;