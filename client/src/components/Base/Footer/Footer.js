import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { BsInfoCircleFill } from 'react-icons/bs'


const Footer = () => {

  return (
    <div
      style={{
        backgroundColor: 'white',
        padding: '5px',
        fontFamily: 'Raleway',
        fontSize: '10pt',
        paddingLeft: '20px',
        paddingRight: '20px',
        color: '#900C3F',
        position: 'fixed',
        bottom: '0',
        width: '100%'
      }}>
      <Row>
        <Col xs={10}>
          Copyright 2020© Daniel Cohen and Nikol Dobreva. All rights reserved.
          </Col>
        <Col style={{ textAlign: 'right' }}>
          <Link to={'/about'} href={'/about'}>
            <BsInfoCircleFill style={{ color: '#900C3F' }} />
          </Link> About
        </Col>
      </Row>
    </div>
  )
}

export default Footer;