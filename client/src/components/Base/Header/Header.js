import React from 'react';
import { Container } from 'react-bootstrap';
import Navigation from '../Navigation/Navigation';

const Header =  () => {
  return (
    <Container fluid>
      <Navigation /> 
    </Container>
  )
}

export default Header;