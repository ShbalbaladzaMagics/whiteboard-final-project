import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "src/models/user.entity";
import { UserService } from "./user.service";
import { TransformService } from "./transform.service";
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from "src/data/constants/secret";
import { JwtStrategy } from "src/strategy/jwt-strategy";
import { AuthService } from "./auth.service";
import { Token } from "src/models/token.entity";
import { BoardService } from "./board.service";
import { Board } from "src/models/board.entity";
import { Lane } from "src/models/lane.entity";
import { Card } from "src/models/card.entity";
import { CommentService } from "./comment.service";
import { Comment } from "src/models/comment.entity";


@Module({
  imports: [
    TypeOrmModule.forFeature([User, Token, Board, Lane, Card, Comment]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d',
      }
    }),
  ],
  providers: [UserService, TransformService, JwtStrategy, AuthService, BoardService, CommentService],
  exports: [UserService, AuthService, BoardService, CommentService]
})

export class ServiceModule { }