export class JWTPayload {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  theme: string;
}
