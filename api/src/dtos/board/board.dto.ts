import { Comment } from "src/models/comment.entity";
import { Lane } from "src/models/lane.entity";
import { User } from "src/models/user.entity";

export class BoardDto {
  id: number;
  name: string;
  description: string;
  backgroundUrl: string;
  lanes: Lane[];
  users: User[];
  comments: Comment[]
}