import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Lane } from "./lane.entity";

@Entity()
export class Card {

  @PrimaryGeneratedColumn()
  id: number;
 
  @Column()
  title: string;
 
  @Column()
  description: string; 
  
  @Column()
  label: string;

  @Column()
  createdOn: string;

  @Column()
  position: number


  @ManyToOne(type => Lane, lane => lane.cards)
  laneId: number;
}