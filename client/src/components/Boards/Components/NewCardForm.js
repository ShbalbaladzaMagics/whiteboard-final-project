import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  CardForm,
  CardHeader,
  CardRightContent,
  CardTitle,
  CardWrapper,
  Detail
} from 'react-trello/dist/styles/Base'
import EditableLabel from 'react-trello/dist/widgets/EditableLabel'
import { Badge } from 'react-bootstrap'

class NewCardForm extends Component {
  updateField = (field, value) => {
    this.setState({ [field]: value })
  }

  handleAdd = () => {
    this.props.onAdd(this.state)
  }

  render() {
    const { t } = this.props
    return (
      <CardForm style={{ padding: "5px", backgroundColor: "white" }}>
        <CardWrapper style={{ backgroundColor: "white" }}>
          <CardHeader>
            <CardTitle style={{ fontFamily: "Raleway" }}>
              <EditableLabel
                placeholder={t('placeholder.title')}
                onChange={val => this.updateField('title', val)}
                autoFocus />
            </CardTitle>
            <CardRightContent>
              <EditableLabel
                placeholder={t('placeholder.label')}
                onChange={val => this.updateField('label', val)} />
            </CardRightContent>
          </CardHeader>
          <Detail>
            <EditableLabel
              placeholder={t('placeholder.description')}
              onChange={val => this.updateField('description', val)} />
          </Detail>
        </CardWrapper>
        <Badge
          variant="outline-success"
          size="sm" onClick={this.handleAdd}
          style={{ marginLeft: "35%", cursor: "pointer" }}>
          {t('button.Add card')}
        </Badge>
      </CardForm>
    )
  }
}

NewCardForm.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onAdd: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
}

NewCardForm.defaultProps = {
}

export default NewCardForm