import React, { useState } from 'react'
import { BsX } from 'react-icons/bs';
import { Container, Form, Col, Button, Row } from 'react-bootstrap'
import BoardApi from '../Base/Api/BoardApi'

const CreateBoard = ({ close }) => {
  const [board, setBoard] = useState({
    name: '',
    description: 'default',
    lanes: []
  });


  const updateBoardName = name => setBoard({ ...board, name });
  const updateBoardDescription = description => setBoard({ ...board, description });

  const handleCreate = () => {
    if (board.name.length < 4) {
      console.log('board name must be at least 4 chars');
      return;
    }
    BoardApi.addBoard(board);
    window.location.reload();
  }

  return (
    <>
      <BsX
        style={{ float: 'left' }}
        size={32}
        onClick={close} />
      <Container className='create-board' style={{ marginTop: '3%' }}>
        <Row className='justify-content-md-center'>
          <h1 style={{ fontFamily: 'Raleway', 'margin-bottom': '40px' }}>
            Give your board a name...
            </h1>
        </Row>
        <Row className='justify-content-md-center'>
          <Col xs={8} style={{ textAlign: 'center' }}>
            <Form.Control
              name='boardName'
              value={board.name}
              type='text'
              placeholder='Title'
              onChange={e => updateBoardName(e.target.value)} />
            <br />
            <Form.Control
              name='description'
              as='textarea'
              maxLength={255}
              value={board.description}
              type='text'
              placeholder='Description'
              onChange={e => updateBoardDescription(e.target.value)} />
            <br />
            {
              localStorage.getItem('theme') === 'light' ?
                (<Button variant='warning' onClick={handleCreate}>
                  Create
                </Button>) :
                (<Button variant='outline-warning' onClick={handleCreate}>
                  Create
                </Button>)
            }
          </Col>
        </Row><br />
        <Col xs />
      </Container >
    </>
  )
}
export default CreateBoard;