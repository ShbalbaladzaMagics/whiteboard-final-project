import React, { useContext } from 'react';
import { Navbar, Row, Col, Button, Image } from 'react-bootstrap';
import { Link, withRouter } from 'react-router-dom';
import { BsPeopleCircle, BsPower } from 'react-icons/bs'
import AuthContext from '../../../providers/AuthContext';
import PropTypes from 'prop-types';
import Toggle from '../../../theme/ChangeTheme';


const Navigation = ({ history }) => {
  const { user, setLoginState } = useContext(AuthContext)

  const logout = () => {
    setLoginState(null);
    sessionStorage.removeItem('token');
    window.localStorage.removeItem('theme')
    window.location.reload(false)
  }

  return (
    <>
      <Navbar
        bg='light'
        expand='xl'
        style={{
          marginLeft: '-4%',
          marginRight: '-1%',
          padding: '10px'
        }}>
        <Navbar.Brand
          href='/'
          className='justify-content-middle'
          style={{
            marginLeft: '4%',
            fontFamily: 'Raleway',
            fontSize: '22pt'
          }}
        >
          <Image
            src='https://img.pngio.com/analytics-board-presentation-icon-presentation-icon-png-512_512.png'
            height='50'
            className='d-inline-block align-middle'
            alt='Canban logo'
          /><span> </span>
          <span style={{ color: '#900C3F' }}>Can</span><span>board</span></Navbar.Brand>
        {user ? (
          <>
            <Navbar.Collapse className='justify-content-end'>
              <Row>
                <Col>
                  <Link to={`/boards`} href={`/boards`} className='justify-content-end'>
                    <Button
                      variant='outline-dark'
                      style={{ paddingBottom: '4px', paddingTop: '4px' }}>
                      Boards
                    </Button>
                  </Link>
                </Col>
                <Col>
                  <Toggle id={user.id} />
                </Col>
                <Col>
                  <Link to={`/profile/${user.id}`} href={`/profile/${user.id}`} className='justify-content-end'>
                    <BsPeopleCircle
                      style={{
                        color: '#900C3F',
                        cursor: 'pointer',
                        fontSize: '16pt'
                      }} />
                  </Link>
                </Col>
                <Col>
                  <BsPower
                    className='justify-content-end'
                    style={{
                      color: '#900C3F',
                      cursor: 'pointer',
                      fontSize: '16pt'
                    }} onClick={logout} />
                </Col>
                <Col></Col>
              </Row>
            </Navbar.Collapse>
          </>) :
          <Navbar.Collapse className='justify-content-end'>
            <Row>
              <Col>
                <Link to={`/login`} href={`/login`} className='justify-content-end'>
                  <Button variant='outline-success'>Login</Button>
                </Link>
              </Col>
              <Col>
                <Link to={`/register`} href={`/register`} className='justify-content-end'>
                  <Button variant='outline-success'>Register</Button>
                </Link>
              </Col>
              <Col></Col>
            </Row>
          </Navbar.Collapse>}
      </Navbar>
    </>
  )
}

Navigation.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(Navigation);