import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { NewCommentDTO } from "src/dtos/board/comment.dto";
import { Board } from "src/models/board.entity";
import { Comment } from "src/models/comment.entity";
import { User } from "src/models/user.entity";
import { Repository } from "typeorm";

@Injectable()
export class CommentService {
  constructor(@InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Board) private readonly boardRepository: Repository<Board>,
    @InjectRepository(Comment) private readonly commentRepository: Repository<Comment>) { }

  public async newComment(userId: number, boardId: string, newComment: NewCommentDTO) {
    const user = await this.getUser(userId);
    const board = await this.getBoard(+boardId);

    const comment = this.commentRepository.create(newComment);
    comment.author = user;
    comment.forBoard = board;
    const createdComment = await this.commentRepository.save(comment);
    return createdComment;
  }

  public async allComments(boardId: string) {
    const board = await this.getBoard(+boardId);

    return board.comments
  }

  async getUser(userId: number): Promise<User> {
    const user = await this.userRepository.findOne({
      where: {
        id: userId
      },
      relations: ['boards', 'comments']
    });
    if (!user) {
      throw new Error(`No user matches the given criteria`)
    }

    return user;
  }

  async getBoard(boardId: number): Promise<Board> {
    const board = await this.boardRepository.findOne({
      where: {
        id: boardId
      },
      relations: ['users', 'comments']
    });
    if (!board) {
      throw new Error(`No user matches the given criteria`)
    }

    return board;
  }
}