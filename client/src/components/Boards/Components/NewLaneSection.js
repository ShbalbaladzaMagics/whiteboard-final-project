import React from 'react'
import { BsPlusCircleFill } from 'react-icons/bs';
import { NewLaneSection } from 'react-trello/dist/styles/Base'

const theme = localStorage.getItem('theme');

export default ({ t, onClick }) => (
  <NewLaneSection
    style={theme === 'light' ?
      {
        backgroundColor: "transparent",
        width: "0px"
      } :
      {
        backgroundColor: "transparent",
        width: "0px"
      }}>
    {/* eslint-disable-next-line */}
    <BsPlusCircleFill
      style={{
        fontSize: "20pt"
    // eslint-disable-next-line
      },
        theme === 'light' ?
          {
            color: "#363537",
            fontSize: "20pt"
          } : {
            color: "#E2E2E2",
            fontSize: "20pt"
          }}
      onClick={onClick}>
      {t('Add new lane')}</BsPlusCircleFill>
  </NewLaneSection>
)