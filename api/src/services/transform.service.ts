import { User } from "src/models/user.entity";
import { ReturnUserDTO } from "src/dtos/user/user-return.dto";
import { UserRole } from "src/models/enums/user-role.enum";

export class TransformService {
  
  toResponceUserDTO(user: User): ReturnUserDTO {
    return({
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      company: user.company,
      companyRole: user.companyRole,
      bio: user.bio,
      avatarUrl: user.avatarUrl, 
      theme: user.theme,
      boards: user.boards ? user.boards : null
    }
    )
  }
}