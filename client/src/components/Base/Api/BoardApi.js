import { BASE_URL } from '../../../providers/base-url'

const BoardApi = {

  removeBoard: (boardId) => {
    fetch(`${BASE_URL}/board/${boardId}`, {
      method: 'DELETE',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
    })  
    .then(response => response.json())
    .then(result => {
      console.log('Board Removed');
      console.log(result);
      return result;
    })
    .catch(err => console.log(err.message));
  },
  addBoard: (board) => {
    fetch(`${BASE_URL}/board`, {
      method: 'POST',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`

      },
      body: JSON.stringify(board)
    })
      .then(response => response.json())
      .then(result => {
        return result;
      })
      .catch(err => console.log(err.message))
  },
  getBoards: () => {
    fetch(`${BASE_URL}/board`, {
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      }
    })
      .then(response => response.json())
      .then(result => {
        return result;
      })
      .catch(err => console.log(err.message))
  },
  getBoard: (boardId) => {
    fetch(`${BASE_URL}/board/${boardId}`, {
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      }
    })
      .then(response => response.json())
      .then(result => {
        return result;
      })
      .catch(err => console.log(err.message))
  },
  updateBoard: (board, { lanes }) => {
    const boardDto = {...board, lanes}

    fetch(`${BASE_URL}/board/${board.id}`, {
      method: 'PUT',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify(boardDto)
    })
      .then(response => response.json())
      .then(result => {
        console.log(result);
        //return result;
      })
      .catch(err => console.log(err.message))
  },
  addLane: (boardId, laneDto) => {
    fetch(`${BASE_URL}/board/${boardId}/lanes`, {
      method: 'POST',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify(laneDto)
    })
      .then(response => response.json())
      .then(result => {
        console.log('Lane Added');
        return result;
      })
      .catch(err => console.log(err.message));
  },
  removeLane: (boardId, lane) => {
    fetch(`${BASE_URL}/board/${boardId}/lanes`, {
      method: 'DELETE',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify(lane)
    })  
    .then(response => response.json())
    .then(result => {
      console.log('Lane Removed');
      console.log(result);
      return result;
    })
    .catch(err => console.log(err.message));
  },
  addCard: (boardId, card) => {
    fetch(`${BASE_URL}/board/${boardId}/cards`, {
      method: 'POST',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify(card)
    })
      .then(response => response.json())
      .then(result => {
        console.log('Card Added');
        return result;
      })
      .catch(err => console.log(err.message));
  },
  removeCard: (boardId, cardDeleteDto) => {
    fetch(`${BASE_URL}/board/${boardId}/cards`, {
      method: 'DELETE',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify(cardDeleteDto)
    })  
    .then(response => response.json())
    .then(result => {
      console.log('Card Removed');
      console.log(result);
      return result;
    })
    .catch(err => console.log(err.message));
  },
  updateCard: (boardId, cardUpdateDto) => {
    fetch(`${BASE_URL}/board/${boardId}/cards`, {
      method: 'PUT',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify(cardUpdateDto)
    })
      .then(response => response.json())
      .then(result => {
        console.log('Card Updated');
        //return result;
      })
      .catch(err => console.log(err.message))
  },
  
}

export default BoardApi