import React from 'react'
import { Container, Image } from 'react-bootstrap'
import './Landing.css'

const Landing = () => {

  return (
    <div style={{ marginTop: '-1%' }}>
      <div className='welcomePhoto'>
        <Image
          className='d-block w-100 imаgе'
          src='https://i.imgur.com/ik9JQ8e.jpg'
          alt='image'
        />
        <div className='middle'>
          <div
            className='text'
            style={{ fontFamily: 'Raleway', marginTop: '15%' }}>
            <h1 style={{ fontWeight: 'bold', fontSize: '50pt' }}>Canboard</h1>
            <h3>Co-working has never been easier.</h3>
            <h3>And everyone <span style={{ fontWeight: 'bold' }}> can </span>do it.</h3>
          </div>
        </div>
      </div>
      <br />
      <Container
        style={{
          width: '45%',
          marginBottom: '1%',
          fontFamily: 'Raleway',
          textAlign: 'justify',
          textAlignLast: 'center'
        }}>
        A simple online collaborative tool, which allows you to manage both you work and personal tasks.
        Stay intact with this an online board to plan, coordinate and discuss and explain complicate plans with the help of visual representation.
        You can create and customize your own Kanban boards or invite your colleagues or friends to join you in the fun times of planning. Exchange tips with them and organize tasks - simple as that.
        <br /><br />As if you are in the same room together!
      <br />
      </Container>
      <br />
      <br />
    </div>
  )
}

export default Landing;