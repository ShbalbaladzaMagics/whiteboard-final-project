export class UpdateUserDTO {
  firstName: string;
  lastName: string;
  company: string;
  bio: string;
}