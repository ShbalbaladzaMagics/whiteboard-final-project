import React, { useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import AuthContext, { extractUser, getToken } from './providers/AuthContext';
import Login from './components/Authentication/Login/Login';
import Register from './components/Authentication/Register/Register';
import Profile from './components/Profile/Profile';
import Header from './components/Base/Header/Header';
import SingleBoard from './components/Boards/SingleBoard';
import './index.css'
import GuardedRoute from './providers/GuardedRoute';
import UpdateProfile from './components/Profile/UpdateProfile';
import { ThemeProvider } from 'styled-components';
import { lightTheme, darkTheme } from './theme/theme';
import { GlobalStyles } from './theme/global';
import { Container } from 'react-bootstrap';
import Landing from './components/Base/Landing/Landing';
import AllBoards from './components/Boards/AllBoards';
import Footer from './components/Base/Footer/Footer';
import About from './components/Base/About/About';

const App = () => {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken())
  });

  const theme = window.localStorage.getItem('theme');
  console.log(theme)

  return (
    <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
      <Router>
        <Header />
        <ThemeProvider theme={theme === 'dark' ? darkTheme : lightTheme}>
          <Container style={{textAlign: 'right', marginTop: '1%'}}>
            <GlobalStyles />
          </Container>
          <Route exact path='/login' component={Login}></Route>
          <Route exact path='/register' component={Register}></Route>
          <Route exact path='/' component={Landing}></Route>
          <Route exact path='/about' component={About}></Route>
          <GuardedRoute exact path='/Board' component={SingleBoard}></GuardedRoute>
          <GuardedRoute exact path='/profile/:id' component={Profile}></GuardedRoute>
          <GuardedRoute exact path='/update/:id' component={UpdateProfile}></GuardedRoute>
          <GuardedRoute exact path='/boards/:id' component={SingleBoard}></GuardedRoute>
          <GuardedRoute exact path='/boards/' component={AllBoards}></GuardedRoute>
        </ThemeProvider>
      <Footer />
      </Router>

    </AuthContext.Provider>
  );
}

export default App;