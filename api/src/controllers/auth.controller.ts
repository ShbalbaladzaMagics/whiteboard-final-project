import { Controller, Post, Body, Delete } from "@nestjs/common";
import { AuthService } from "src/services/auth.service";
import { LoginDTO } from "src/dtos/user/login.dto";
import { GetToken } from "src/auth/get-token.decorator";

@Controller('session')
export class AuthController {

  constructor(
    private readonly authService: AuthService,
  ) {}

  @Post('login')
  async login(@Body() user: LoginDTO): Promise<{ token: string }> {
    return await this.authService.login(user);
  }

  @Delete('logout')
  async logout(@GetToken() token: string): Promise<{ message: string }> {
    await this.authService.blacklist(token?.slice(7));
    return {
      message: `You have been loged out`
    }
  }

}
