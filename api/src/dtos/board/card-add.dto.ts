export class CardAddDto {
  title: string;
  description: string;
  label: string;
  laneId: number;
}