import React, { useState, useContext } from 'react';
import { withRouter } from 'react-router';
import { Form, Button, Container, Col, Row, InputGroup } from 'react-bootstrap';
import { BsFillShieldLockFill, BsFillEnvelopeFill, BsPersonFill } from 'react-icons/bs'
import { BASE_URL } from '../../../providers/base-url';
import AuthContext from '../../../providers/AuthContext';

const Register = ({ history }) => {
  const [newUser, setNewUser] = useState({})
  const [confirmPassword, setConfirmPassword] = useState('');

  const updateEmail = email => setNewUser({ ...newUser, email });
  const updatePassword = password => setNewUser({ ...newUser, password });
  const updateFirst = firstName => setNewUser({ ...newUser, firstName });
  const updateLast = lastName => setNewUser({ ...newUser, lastName })

  const register = () => {
    if (newUser.password !== confirmPassword) {
      alert(`Your passwords don't match!`);
      return;
    }

    fetch(`${BASE_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newUser),
    })
      .then(response => response.json())
      .then(result => {
      })
      .catch(error => console.log(error.message))
      .finally(history.push('/login'))
  }

  const { user } = useContext(AuthContext);
  if (user) {
    history.push('/')
  }

  return (
    <Container style={{ marginTop: '3%' }}>
      <h1 style={{ fontFamily: 'Raleway', color: '#522150' }}>Register</h1>
      <br />
      <div>
        <Row>
          <Col xs={10} md={5}>
            <InputGroup className='mb-2 mr-sm-2'>
              <InputGroup.Prepend>
                <InputGroup.Text><BsFillEnvelopeFill /></InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control
                name='email'
                type='email'
                placeholder='Email' onChange={e => updateEmail(e.target.value)} />
            </InputGroup>
          </Col>
        </Row>
        <br />
        <Row>
          <Col xs={10} md={5}>
            <InputGroup className='mb-2 mr-sm-2'>
              <InputGroup.Prepend>
                <InputGroup.Text><BsFillShieldLockFill /></InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control
                name='password'
                type='password'
                placeholder='Password'
                onChange={e => updatePassword(e.target.value)} />
            </InputGroup>
          </Col>
          <Col xs={10} md={5}>
            <InputGroup className='mb-2 mr-sm-2'>
              <InputGroup.Prepend>
                <InputGroup.Text><BsFillShieldLockFill /></InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control
                name='confirmPassword'
                type='password'
                placeholder='Confirm password'
                onChange={e => setConfirmPassword(e.target.value)} />
            </InputGroup>
          </Col>
        </Row>
        <br />
        <Row>
          <Col xs={10} md={5}>
            <InputGroup className='mb-2 mr-sm-2'>
              <InputGroup.Prepend>
                <InputGroup.Text><BsPersonFill /></InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control
                name='firstName'
                type='text'
                placeholder='First Name'
                onChange={e => updateFirst(e.target.value)} />
            </InputGroup>
          </Col>
          <Col xs={10} md={5}>
            <InputGroup className='mb-2 mr-sm-2'>
              <InputGroup.Prepend>
                <InputGroup.Text><BsPersonFill /></InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control
                name='lastName'
                type='text'
                placeholder='Last Name'
                onChange={e => updateLast(e.target.value)} />
            </InputGroup>
          </Col>
        </Row>
        <br />
      </div>
      <Button style={{ backgroundColor: '#522150' }} type='submit' onClick={register}>Register</Button>
    </Container >
  )

}

export default withRouter(Register); 