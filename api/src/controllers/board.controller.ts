import { Controller, Post, Body, Get, Param, UseGuards, Put, Delete, UseInterceptors, UploadedFile } from "@nestjs/common";
import { BoardDto } from '../dtos/board/board.dto';
import { BoardService } from "src/services/board.service";
import { UserId } from "src/auth/user-id.decorator";
import { LaneDto } from "src/dtos/board/lane.dto";
import { CardAddDto } from "src/dtos/board/card-add.dto";
import { BlacklistGuard } from "src/auth/blacklist.guard";
import { CardUpdateDto } from "src/dtos/board/card-update.dto";
import { CardDeleteDto } from "src/dtos/board/card-delete.dto";
import { CommentService } from "src/services/comment.service";
import { NewCommentDTO } from "src/dtos/board/comment.dto";
import { Comment } from "src/models/comment.entity";
import { FileInterceptor } from "@nestjs/platform-express";
import { diskStorage } from 'multer';
import { extname } from "path";



@Controller('board')
export class BoardController {
  constructor(private readonly boardService: BoardService, private readonly commentService: CommentService) { }

  // Board CRUD


  @Post()
  @UseGuards(BlacklistGuard)
  async create(@Body() board: BoardDto, @UserId() userId: number) {
    return this.boardService.createBoard(board, userId);
  }

  @Get(':id')
  @UseGuards(BlacklistGuard)
  async getOne(@Param('id') boardId: string, @UserId() userId: number) {
    return this.boardService.getBoard(+boardId);
  }

  @Get()
  @UseGuards(BlacklistGuard)
  async getAll(@UserId() userId: number) {
    return this.boardService.getBoards(userId);
  } // GET all boards of the requesting user

  @Delete(':id')
  @UseGuards(BlacklistGuard)
  async remove(@Param('id') boardId: string, @UserId() userId: number) {
    return this.boardService.removeBoard(+boardId)
  }

  @Put(':id')
  @UseGuards(BlacklistGuard)
  async updateBoard(@Param('id') boardId: string, @Body() board: BoardDto) {
    return this.boardService.updateBoard(+boardId, board);
  }

  //Lanes CRUD

  @Post(':id/lanes')
  @UseGuards(BlacklistGuard)
  async addLane(@Param('id') boardId: string, @Body() lane: LaneDto) {
    return this.boardService.addLane(+boardId, lane);
  }

  @Delete(':id/lanes')
  @UseGuards(BlacklistGuard)
  async removeLane(@Param('id') boardId: string, @Body() lane: { id: number }) {
    return this.boardService.removeLane(+boardId, lane.id);
  }

  // Cars CRUD

  @Post(':id/cards')
  @UseGuards(BlacklistGuard)
  async addCard(@Param('id') boardId: string, @Body() card: CardAddDto) {
    return await this.boardService.addCard(+boardId, card);
  }

  @Delete(':id/cards')
  @UseGuards(BlacklistGuard)
  async removeCard(@Param('id') boardId: string, @Body() card: CardDeleteDto) {
    return await this.boardService.removeCard(+boardId, card);
  }

  @Put(':id/cards')
  @UseGuards(BlacklistGuard)
  async updateCard(@Param('id') boardId: string, @Body() card: CardUpdateDto) {
    return await this.boardService.updateCard(card)
  }

  //Comments
  @UseGuards(
    BlacklistGuard
  )
  @Post(':boardId/comment')
  async createComment(@Param('boardId') boardId: string, @UserId() userId: number, @Body() body: NewCommentDTO): Promise<Comment> {
    return await this.commentService.newComment(userId, boardId, body);
  }

  @UseGuards(
    BlacklistGuard
  )
  @Get(':boardId/comments')
  async getAllComments(@Param('boardId') boardId: string, @Body() body: NewCommentDTO): Promise<Comment[]> {
    return await this.commentService.allComments(boardId)
  }

  @Put(':boardId/users')
  async addUsers(@Param('boardId') boardId: string, @Body() body: {email: string}) {
    return await this.boardService.inviteUsers(boardId, body)
  }

  @Put(':id/background')
  @UseInterceptors(
    FileInterceptor('files', {
      storage: diskStorage({
        destination: './avatars',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          console.log('filename cb', file);
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          //Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadBackground(@UploadedFile() files, @Param('id') id: string) {
    return await this.boardService.updateBackground(id, files.filename);
  }
}
