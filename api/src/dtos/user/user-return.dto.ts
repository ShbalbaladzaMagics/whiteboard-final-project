import { Board } from "src/models/board.entity";

export class ReturnUserDTO {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  company: string;
  companyRole: string;
  bio: string;
  avatarUrl: string;
  theme: string;
  boards: Board[]
} 