import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "src/models/user.entity";
import { Repository, MoreThanOrEqual } from "typeorm";
import { Board } from "src/models/board.entity";
import { Lane } from "src/models/lane.entity";
import { Card } from "src/models/card.entity";
import { BoardDto } from "src/dtos/board/board.dto";
import {getConnection} from "typeorm";
import { LaneDto } from "src/dtos/board/lane.dto";
import { CardAddDto } from "src/dtos/board/card-add.dto";
import { CardUpdateDto } from "src/dtos/board/card-update.dto";
import { CardDeleteDto } from "src/dtos/board/card-delete.dto";
var moment = require('moment'); // require


@Injectable()
export class BoardService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Board) private readonly boardRepository: Repository<Board>,
    @InjectRepository(Lane) private readonly laneRepository: Repository<Lane>,
    @InjectRepository(Card) private readonly cardRepository: Repository<Card>
  ) {}

  public async createBoard(board: BoardDto, userId: number): Promise<BoardDto> {

    const user = await this.userRepository.findOne(userId);

    const newBoard = new Board();

    newBoard.name = board.name;
    newBoard.description = board.description;
    newBoard.lanes = [];
    newBoard.users = [user];
    
    await this.boardRepository.save(newBoard);

    return newBoard as BoardDto;
  }

  public async getBoard(boardId: number) {
    const board = await this.boardRepository.findOne({
      where: {
        id: boardId ,
        isDeleted: false
      },
      // select: ['id', 'name'],
      relations: ['lanes', 'users', 'lanes.cards', 'comments', 'comments.author']
    });
    return board;

  }

  public async getBoards(userId: number) {
    
    const user = await this.userRepository.findOne(userId, {
      where: {
        id: userId
      },
      relations: ["boards", "boards.lanes", "boards.lanes.cards", "boards.comments"]
    });
    const userBoards = user.boards.filter((board) => !board.isDeleted)
    user.boards = userBoards;
    return user.boards;
  }

  public async inviteUsers (boardId: string, newUser: {email: string}) {
    const board = await this.boardRepository.findOne({
      where: {id: +boardId},
      relations: ['lanes', 'users', 'lanes.cards', 'comments', 'comments.author']
    });

    const user = await this.userRepository.findOne({
      where: {email: newUser.email},
      relations: ["boards", "boards.lanes", "boards.lanes.cards", "boards.comments"]
    });

    board.users.push(user);
    await this.boardRepository.save(board);
    return `User added!`
  } 

  async updateBackground(id: string, filename: string) {
    const board = await this.boardRepository.findOne(+id);

    board.backgroundUrl = filename;
    return await this.boardRepository.save(board);
}

  public async updateBoard(boardId: number, boardData: BoardDto) {
  //   const board = await this.getBoard(boardId);
  //   console.log('board ---', board,'boarData ---', boardData);
  //   const boardLanes = boardData.lanes.map(async (laneData) => {
  //     const lane = await this.laneRepository.findOne(laneData.id, {
  //       relations: ['cards']
  //     });
  //     if (lane === undefined){
  //       await this.laneRepository.save(lane)
  //     }
  //   });
  //   board.name = boardData.name;
  //   board.lanes = boardData.lanes;

  //   await this.boardRepository.save(board);
  //   const res = board.lanes.map(async (lane) => { await this.updateLane(lane) })
  //   console.log(res);
  }

  public async removeBoard(boardId: number) {
    const board = await this.boardRepository.findOne({
      where: {id: boardId},
    })
    board.isDeleted = true;
    return await this.boardRepository.save(board);
  }

  public async updateLane(lane: Lane) {
    await this.laneRepository.save(lane)

    lane.cards.map(async (card) => {
      const cardDto = new CardUpdateDto();
      cardDto.id = card.id;
      cardDto.title = card.title;
      cardDto.description = card.description;
      cardDto.label = card.label;
      cardDto.laneId = lane.id;
  
      await this.updateCard(cardDto)
    });

  }

  public async addLane(boardId: number, laneDto: LaneDto) {
    
    const lane = new Lane();
    lane.title = laneDto.title;
    lane.label = laneDto.label;
    lane.boardId = boardId;
    lane.cards = [];

    return await this.laneRepository.save(lane);
  }

  public async removeLane(boardId: number, laneId: number) {
    await getConnection()
    .createQueryBuilder()
    .relation(Board, "lanes")
    .of(boardId) 
    .remove(laneId);

    return {"message": "Lane removed!"};
  }

  public async addCard(boardId: number, cardDto: CardAddDto) { 
    const lane =  await this.laneRepository.findOne(cardDto.laneId, {
      relations: ['cards']
    });
    const card = new Card();

    card.title = cardDto.title;
    card.label = cardDto.label;
    card.description = cardDto.description;
    card.createdOn = moment().format('HH:mm DD-MM-YYYY');
    card.laneId = cardDto.laneId;
    card.position = lane.cards.length;

    return await this.cardRepository.save(card);
  }

  public async removeCard(boardId: number, card: CardDeleteDto) {
    
    await getConnection()
    .createQueryBuilder()
    .relation(Lane, "cards")
    .of(card.laneId) 
    .remove(card.id);

    return {"message": "Card removed!"};
   }

   public async updateCard(cardDto: CardUpdateDto) {

    const card = await this.cardRepository.findOne(cardDto.id)

    const restCards = await this.cardRepository.find({
      where: {
        laneId: cardDto.laneId,
        position: MoreThanOrEqual(cardDto.position)
      }
    });

    const restCardsCopy = restCards.map((card) => {
      card.position ++;
      return card;
    });

    await getConnection()
    .createQueryBuilder()
    .relation(Lane, "cards")
    .of(cardDto.sourceLaneId) 
    .remove(card.id);
 
   
    card.laneId = cardDto.laneId;
    card.position = cardDto.position;
    
    restCardsCopy.map(async (card) => {
      await this.cardRepository.save(card);
      return card;
    }); 
    const result =  await this.cardRepository.save(card);
    return result
  }
}
