import React, { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import AuthContext from './AuthContext';

const GuardedRoute = ({ component: RouteComponent, ...rest }) => {
  
  const {user} = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={routeProps =>
        !!user ? (
          <RouteComponent {...routeProps} />
        ) : (
          <Redirect to={"/"} />
        )
      }
    />
  );
};

export default GuardedRoute;