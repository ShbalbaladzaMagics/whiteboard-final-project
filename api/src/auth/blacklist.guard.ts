import { AuthGuard } from "@nestjs/passport";
import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { AuthService } from "src/services/auth.service";

@Injectable()
export class BlacklistGuard extends AuthGuard('jwt') implements CanActivate {

    constructor(
        private readonly auhtService: AuthService,
    ) {
        super();
    }

    public async canActivate(ctx: ExecutionContext): Promise<boolean> {

        const basicActivation = await super.canActivate(ctx);
        if(!basicActivation){
            return false;
        }

        const request = ctx.switchToHttp().getRequest();
        if (request.headers.authorization === undefined) {
            return false;
        }
        const token = request.headers.authorization.slice(7);
        
        const isBadToken =  await this.auhtService.isBlacklisted(token);

        return !isBadToken;
    }
}