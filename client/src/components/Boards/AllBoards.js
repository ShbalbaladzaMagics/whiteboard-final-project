import React, { useContext, useEffect, useState } from 'react'
import { Card, CardDeck, Col, Image } from 'react-bootstrap';
import { BsArrowRight, BsPlusCircleFill } from 'react-icons/bs';
import { Link } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import { BASE_URL } from '../../providers/base-url';
import CreateBoard from './CreateBoard';

const AllBoards = () => {
  const [boards, setBoards] = useState([]);
  const { user } = useContext(AuthContext);
  const [isAddBoardVisible, toggleAddBoard] = useState(false);

  useEffect(() => {
    fetch(`${BASE_URL}/board`, {
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      }
    })
      .then(response => response.json())
      .then(result => {
        setBoards(result);
      })
      .catch(err => console.log(err.message))
  }, []);

  const toggleCreateBoardComponent = () => toggleAddBoard(!isAddBoardVisible);

  if (isAddBoardVisible) {
    return (
      <CreateBoard close={toggleCreateBoardComponent} />

    )
  }

  return (
    <div
      style={{ marginTop: '2%', width: '95%' }} >
      <h1 style={{ textAlign: 'center', fontFamily: 'Raleway' }}>
        All your boards in just one place!
      </h1>
      <BsPlusCircleFill
        size={24}
        onClick={toggleCreateBoardComponent}
        style={{
          cursor: 'pointer',
          marginLeft: '1%'
        }} />
      <br />
      {user ? (
        <CardDeck
          style={{
            width: '95%',
            margin: 'auto',
            fontFamily: 'Raleway'
          }}>
          {boards.map((board) => {
            return (
              <Col xs={12} md={6} xl={3} lg={3} key={board.id}>
                <Card style={{
                  border: '1px solid #900C3F',
                  marginBottom: '15px',
                  width: '100%',
                  height: '180px'
                }}>
                  <Card.Header
                    style={{
                      padding: '5px',
                      paddingBottom: '0px',
                      textAlign: 'center',
                      color: '#363537'
                    }}>
                    {board.name}
                  </Card.Header>
                  <Card.Body
                    style={{
                      textAlign: 'center',
                      borderTop: '1px solid #900C3F',
                      margin: '0px',
                      padding: '0px'
                    }}>
                    {board.backgroundUrl &&
                      <Image src={`${BASE_URL}/${board.backgroundUrl}`}
                        style={{ width: '100%', height: '100%' }}
                        rounded />}
                  </Card.Body>
                  <Card.Footer
                    style={{
                      color: '#900C3F',
                      textAlign: 'right',
                      paddingRight: '10px',
                      backgroundColor: 'lightgray',
                      paddingTop: '2px',
                      paddingBottom: '2px'
                    }}>
                    <Link to={`/boards/${board.id}`} href={`/boards/${board.id}`} style={{ color: '#900C3F' }}>
                      Board<BsArrowRight />
                    </Link>
                  </Card.Footer>
                </Card>
                <br />
                </Col>
            )
          })}
        </CardDeck>) : null}
    </div>
  )
}

export default AllBoards;