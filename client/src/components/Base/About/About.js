import React from 'react'
import { Container, Image, Row } from 'react-bootstrap';
import { BsInfoCircleFill } from 'react-icons/bs';

const About = () => {

  return (
    <>
      <Container style={{ fontFamily: 'Raleway', width: '30%' }}>
        <Row className='justify-content-md-center'>
          <h1 style={{ color: '#900C3F' }}> <BsInfoCircleFill style={{ fontSize: '15pt' }} /> About</h1>
        </Row>
        <Row
          className='justify-content-md-center'
          style={{
            width: '150%',
            marginLeft: '-20%',
            paddingTop: '10px',
            fontVariant: 'small-caps'
          }}
        >
          <h2 style={{ fontSize: '12pt' }}>
            this project was assigned to us by
             <Image src='https://dev.bg/wp-content/uploads/2016/12/Tick42_CMYK_72dpi.png' height='50px' />
             </h2>
        </Row>
        <br />
        <Row>
          <h2>
            <Image src='https://i.imgur.com/i9yW4Ww.png' height='50px' /> ReactJS
          </h2>
        </Row>
        <Row className='justify-content-end'>
          <h2>
            react-trello <Image src='https://i.imgur.com/W5Pp1mN.png' height='50px' />
          </h2>
        </Row>
        <br />
        <Row>
          <h2>
            <Image src='https://i.imgur.com/Udn76Hd.png' height='50px' /> JavaScript
            </h2>
        </Row>
        <br />
        <Row className='justify-content-end'>
          <h2>
            React-Bootstrap <Image src='https://i.imgur.com/c6xndua.png' height='50px' />
          </h2>
        </Row>
        <br />
        <Row >
          <h2>
            <Image src='https://i.imgur.com/Z6awlvt.png' height='70px' />
             NestJS</h2>
        </Row>
        <br />

        <Row className='justify-content-end'>
          <h2>
            TypeScript <Image src='https://i.imgur.com/Xi6UGDm.png' height='50px' />
          </h2>
        </Row>
        <br />
        <Row>
          <h2>
            <Image src='https://i.imgur.com/YKC4BFM.png' height='50px' /> TypeORM
            </h2>
        </Row>
        <br />
        <Row className='justify-content-end'>
          <h2>
            JWT <Image src='https://devonblog.com/wp-content/uploads/2018/08/jwt_05.jpg' height='50px' />
          </h2>
        </Row>
        <br />
      </Container>
      <br />
    </>
  )

}

export default About;