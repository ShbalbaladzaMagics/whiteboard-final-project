// Toggle.js
import React, { useEffect, useState } from 'react'
import { BsSun } from 'react-icons/bs';
import { BsMoon } from 'react-icons/bs';
import { BASE_URL } from '../providers/base-url';

const Toggle = ({ id }) => {
  const [userProfile, setUsersProfile] = useState([])
  useEffect(() => {
    fetch(`${BASE_URL}/users/${id}`,
      {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`,
        }
      })
      .then(result => {
        return result.json();
      })
      .then(result => {
        if (result) {
          setUsersProfile(result);
        }
      })
      .catch(err => console.log(err.message))
    // eslint-disable-next-line
  }, []);

  const newTheme = userProfile.theme === 'light' ? 'dark' : 'light';
  const changeTheme = () => {
    fetch(`${BASE_URL}/users/${userProfile.id}/theme`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ theme: newTheme }),
    })
      .then(response => response.json())
      .then(result => {
        console.log(result.theme);
        window.localStorage.removeItem('theme')
        window.localStorage.setItem('theme', result.theme)
        reload()
      })
      .catch(error => console.log(error.message))
  }

  const reload = () => {
    window.location.reload(false)
  }

  return (
    <>
      {userProfile.theme === 'light' ?
        <BsMoon
          onClick={changeTheme}
          style={{
            cursor: 'pointer',
            fontSize: '16pt',
            color: '#900C3F'
          }} /> :
        <BsSun
          onClick={changeTheme}
          style={{
            cursor: 'pointer',
            color: '#900C3F',
            fontSize: '16pt'
          }} />
      }
    </>
  );
};

export default Toggle;