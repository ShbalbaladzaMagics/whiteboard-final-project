import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Board } from "./board.entity";
import { User } from "./user.entity";

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  comment: string;

  @ManyToOne(
    type => User,
    user => user.comments
  )
  author: User;

  @ManyToOne(
    type => Board,
    board => board.comments
  )
  forBoard: Board;
}