import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { Request } from "express";

export const GetToken = createParamDecorator((data: any, ctx: ExecutionContext) => {

    const req = ctx.switchToHttp().getRequest();

    return req.rawHeaders[1]
})