import React, { useState } from 'react'
import { Container, Form, Col, Button, Image } from 'react-bootstrap'
import './Profile.css'
import { BASE_URL } from '../../providers/base-url';

const UpdateProfile = ({ toUpdate }) => {

  const [user, setUser] = useState({
    firstName: toUpdate.firstName,
    lastName: toUpdate.lastName,
    company: toUpdate.company,
    bio: toUpdate.bio,
    avatarUrl: toUpdate.avatarUrl
  })

  const [files, setFiles] = useState([]);

  const updateFirst = firstName => setUser({ ...user, firstName });
  const updateLast = lastName => setUser({ ...user, lastName });
  const updateCompany = company => setUser({ ...user, company });
  const updateBio = bio => setUser({ ...user, bio });

  const update = () => {

    fetch(`${BASE_URL}/users/${toUpdate.id}`, {
      method: 'PUT',
      headers: {
        'CONTENT-TYPE': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`
      },
      body: JSON.stringify(user)
    })
      .then(responce => responce.json())
      .then(result => {
      })
      .catch(err => console.log(err.message))
      .finally(updateAvatar())
  }

  const updateAvatar = () => {
    if (files.length === 0) {
      window.location.reload();
      return;
    }
    const data = new FormData();
    data.append('files', files[0]);
    fetch(`${BASE_URL}/users/${toUpdate.id}/avatar`, {
      method: 'PUT',
      headers: {
      },
      body: data,
    })
      .then(r => r.json())
      .then(console.log)
      .catch(console.warn)
      .finally(window.location.reload(false))
  };

  return (
    <Container className='profile'>
      <Form onSubmit={update} className='justify-content-md-center'>
        <Col xs={5}>
          <Form.Control
            name='firstName'
            value={user.firstName}
            type='text'
            placeholder='First Name'
            onChange={e => updateFirst(e.target.value)} />
        </Col>
        <br />
        <Col xs={5}>
          <Form.Control
            name='lastName'
            value={user.lastName}
            type='text'
            placeholder='Last Name'
            onChange={e => updateLast(e.target.value)} />
        </Col>
        <br />
        <Col xs={5}>
          <Form.Control
            name='company'
            type='text'
            value={user.company}
            placeholder='Company'
            onChange={e => updateCompany(e.target.value)} />
        </Col>
        <br />
        <Col xs={5}>
          <Form.Control
            name='bio'
            type='text'
            value={user.bio}
            maxLength={255}
            placeholder='Bio'
            onChange={e => updateBio(e.target.value)}
            as='textarea'
            rows='4'
            aria-describedby='passwordHelpInline' />
          <Form.Text id='passwordHelpBlock' muted>
            Description cannot be more than 255 characters
           </Form.Text>
        </Col>
      </Form>
      <br />
      <Col md={2}>
        <Image src={`${BASE_URL}/${user.avatarUrl}`} style={{ inlineSize: '100%' }} />
      </Col>
      <Col>
        <br />
        <Form.File type='file' onChange={e => setFiles(Array.from(e.target.files))} />
      </Col>
      <br />
      <Col>
        <Button onClick={update}>
          Update
          </Button>
        <Button onClick={() => (window.location.reload(false))}>
          Cancel
        </Button>
      </Col>
    </Container >
  )
}

export default UpdateProfile;