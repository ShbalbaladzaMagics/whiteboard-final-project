import React, { useState, useContext, useEffect } from 'react';
import { BASE_URL } from '../../providers/base-url';
import { Container, Row, Col, Image, OverlayTrigger, Tooltip, Alert, Card } from 'react-bootstrap';
import './Profile.css'
import { BsArrowRight, BsGear } from 'react-icons/bs';
import UpdateProfile from './UpdateProfile';
import AuthContext from '../../providers/AuthContext';
import { Link } from 'react-router-dom';

const Profile = ({ match }) => {
  const {
    params: { id }
  } = match;
  const [userProfile, setUsersProfile] = useState([])
  const { user } = useContext(AuthContext);

  const [isUpdateVisible, toggleUpdate] = useState(false);

  const theme = localStorage.getItem('theme')

  const defaultPhoto = 'https://www.lococrossfit.com/wp-content/uploads/2019/02/user-icon.png'

  useEffect(() => {
    fetch(`${BASE_URL}/users/${id}`,
      {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token') || ''}`,
        }
      })
      .then(result => {
        return result.json();
      })
      .then(result => {
        if (result) {
          setUsersProfile(result);
        }
      })
      .catch(err => console.log(err.message));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  if (isUpdateVisible) {
    return (
      <>
        <UpdateProfile toUpdate={userProfile} />
      </>
    )
  }

  const tooltipMessage = (props) => (
    <Tooltip id='button-tooltip' {...props}>
      Edit profile
    </Tooltip>
  );

  return (
    <Container className='profile' float='center'>
      <Row>
        <Col md={1} />
        <Col
          md={4}
          className={theme === 'light' ? 'light-theme' : 'dark-theme'}
          style={{
            textAlign: 'right',
            verticalAlign: 'middle'
          }} >
          <br />
          <Image
            roundedCircle
            src={userProfile.avatarUrl ? `${BASE_URL}/${userProfile.avatarUrl}` : defaultPhoto}
            style={{
              textAlign: 'right',
              inlineSize: '60%'
            }} />
          <br />
          <br />
          <h1>{userProfile.firstName} {userProfile.lastName}</h1>
          <h3 >{userProfile.company}</h3>
          <h6> {userProfile.email}</h6>
          {userProfile.id === user.id ? (<>
            <Row className='justify-content-end'>
              <Col xs={5}>
                <OverlayTrigger
                  placement='right'
                  delay={{ show: 250, hide: 50 }}
                  overlay={tooltipMessage}
                >
                  <BsGear
                    onClick={() => toggleUpdate(true)}
                    style={{
                      cursor: 'pointer',
                      fontSize: '16pt'
                    }} />
                </OverlayTrigger>
              </Col>
            </Row>
          </>) : null}
          <br /><Row>
            <Col>
              <Alert
                variant={theme === 'light' ? 'light' : 'dark'}
                style={{
                  textAlign: 'center',
                  fontSize: '11pt'
                }}>
                {userProfile.bio}</Alert>
            </Col>
          </Row>
        </Col>
        <Col xs={4}>
          {userProfile.boards ? userProfile.boards.map((board) => {
            return (
              <Card style={{
                border: '1px solid #900C3F',
                marginBottom: '15px',
                width: '100%',
                height: '180px'
              }}>
                <Card.Header
                  className='da'
                  style={{
                    padding: '5px',
                    paddingBottom: '0px',
                    textAlign: 'center'
                  }}>
                  {board.name}
                </Card.Header>
                <Card.Body style={{
                  textAlign: 'center',
                  borderTop: '1px solid #900C3F',
                  margin: '0px',
                  padding: '0px'
                }}>
                  {board.backgroundUrl &&
                    <Image
                      src={`${BASE_URL}/${board.backgroundUrl}`}
                      style={{ width: '100%', height: '100%' }}
                      rounded />}
                </Card.Body>
                <Card.Footer style={{
                  color: '#900C3F',
                  textAlign: 'right',
                  paddingRight: '10px',
                  backgroundColor: 'lightgray',
                  paddingTop: '2px',
                  paddingBottom: '2px'
                }}>
                  <Link
                    to={`/boards/${board.id}`}
                    href={`/boards/${board.id}`}
                    style={{ color: '#900C3F' }}>
                    Board
                      <BsArrowRight />
                  </Link>
                </Card.Footer>
              </Card>
            )
          }) : null}
        </Col>
      </Row>
      <br />
    </Container>
  )
}
export default Profile;