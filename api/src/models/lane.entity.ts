import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import { Card } from "./card.entity";
import { Board } from "./board.entity";

@Entity()
export class Lane {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Board, board => board.lanes)
  boardId: number;

  @Column()
  title: string;

  @Column()
  label: string;

  @OneToMany( type => Card, card => card.laneId)
  cards: Card[];

}