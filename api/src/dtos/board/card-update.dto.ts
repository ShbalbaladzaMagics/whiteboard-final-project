export class CardUpdateDto {
  id: number;
  title: string;
  description: string;
  label: string;
  laneId: number;
  sourceLaneId: number;
  position: number;
}