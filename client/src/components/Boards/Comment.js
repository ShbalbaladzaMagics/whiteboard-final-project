import React from 'react'
import { Card, Image } from 'react-bootstrap'
import { BASE_URL } from '../../providers/base-url'

const Comment = ({ comment, variant }) => {
  const variantLeft = {
    color: 'black',
    fontFamily: 'Raleway',
    border: 'none',
    borderRadius: '0px',
    textAlign: 'left'
  }
  const variantRight = {
    color: 'black',
    fontFamily: 'Raleway',
    border: 'none',
    borderRadius: '0px',
    textAlign: 'right',
    marginLeft: '5%'
  }

  return (
    <>
      <Card style={variant === '1' ? variantLeft : variantRight}>
        <Card.Header style={{ borderBottom: '1px solid gray', fontSize: '8pt' }}>
          {variant === '1' ?
            (<>
              <Image
                roundedCircle
                src={`${BASE_URL}/${comment.author.avatarUrl}`}
                style={{ inlineSize: '10%' }} />
              <> {comment.author.firstName} {comment.author.lastName}</>
            </>) : (
              <>
                <>
                  {comment.author.firstName} {comment.author.lastName}
                </>
                <Image
                  roundedCircle
                  src={`${BASE_URL}/${comment.author.avatarUrl}`}
                  style={{ inlineSize: '10%' }} />
              </>)
          }
        </Card.Header>
        <Card.Body style={{ fontSize: '10pt' }}>
          {comment.comment}
          </Card.Body>
      </Card>
      <p></p>
    </>
  )
}

export default Comment;