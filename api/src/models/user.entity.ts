import { Entity, PrimaryGeneratedColumn, Column, Unique, ManyToMany, JoinTable, OneToMany } from "typeorm";
import { UserRole } from "./enums/user-role.enum";
import { IsEmail } from 'class-validator'
import { Board } from "./board.entity";
import { Comment } from "./comment.entity";

@Entity()
export class User {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  @IsEmail()
  email: string

  @Column()
  password: string

  @Column()
  firstName: string

  @Column()
  lastName: string

  @Column({ default: null })
  company: string

  @Column({default: null})
  companyRole: string;

  @Column({ default: null })
  bio: string;

  @ManyToMany(type => Board, board => board.users)
  @JoinTable()
  boards: Board[];

  @Column({ nullable: true })
  avatarUrl: string;

  @Column({ default: 'light'})
  theme: string

  @OneToMany(type => Comment, comment => comment.author)
  comments: Comment[]
}